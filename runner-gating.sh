#!/bin/bash

# exit when any command fails
set -e

wget -q https://gitlab.freedesktop.org/freedesktop/test-ci/-/raw/main/users.txt

# ignore more checks for vips
if grep -vqFx $GITLAB_USER_LOGIN users.txt
then
  # user is not a VIP,
  # check if the project is part of a group => success
  # this work either for direct commits in a project part of a group,
  # but also works for Merge Requests open against a project part of a group
  wget -q --output-document /dev/null \
    https://gitlab.freedesktop.org/api/v4/groups/$CI_PROJECT_ROOT_NAMESPACE
fi

rm users.txt

echo "Thank you for contributing to freedesktop.org"

eval $CI_PRE_CLONE_SCRIPT
